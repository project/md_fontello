<?php

namespace Drupal\md_fontello\TwigExtension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MDIcon extends AbstractExtension {

  /**
   * Gets a unique identifier for this Twig extension.
   *
   * @return string
   *   A unique identifier for this Twig extension.
   */
  public function getName() {
    return 'twig.md_icon';
  }

  public function getFunctions() {
    return [
      new TwigFunction('md_icon', $this->renderIcon(...)),
    ];
  }

  public static function renderIcon($name, $icon) {
    $build = [
      '#theme' => 'md_icon',
      '#name' => $name,
      '#icon' => $icon,
    ];
    return $build;
  }

}
