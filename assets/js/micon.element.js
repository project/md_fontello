/**
 * @file
 * Initialize fontIconPicker.
 */

(function (Drupal, $, once) {
  "use strict";

  Drupal.behaviors.mdiconElement = {
    attach: function (context) {
      const $elements = $(
        once("mdiconElement", "select.form-md-icon", context),
      );
      $elements.fontIconPicker();
    },
  };
})(Drupal, jQuery, once);
